/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useCallback, useRef } from 'react';
import mqtt from 'mqtt';
import "./scss/demo.scss";

function App() {
    const [aliasData, setAliasData] = useState({ "Zone1": 0, "Zone2": 0 });
    const aliasBufferRef = useRef({ "Zone1": [], "Zone2": [] });
    const [lastDataReceivedTime, setLastDataReceivedTime] = useState({ "Zone1": Date.now(), "Zone2": Date.now() });
    const [tempAliasData, setTempAliasData] = useState({ "Zone1": 0, "Zone2": 0 });
    const [totalReceivedData, setTotalReceivedData] = useState({ "Zone1": 0, "Zone2": 0 });

    const updateAliasData = useCallback((alias, buffer) => {
        const prunedBuffer = buffer.filter(item => Date.now() - item.timestamp <= 4000);
        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
    }, []);

    const pruneBuffer = useCallback((buffer, alias, otherAliasBuffer) => {
        const currentTime = Date.now();
        const prunedBuffer = [];
        let otherAliasBufferLength = otherAliasBuffer.length;

        for (let i = 0; i < buffer.length; i++) {
            if (currentTime - buffer[i].timestamp <= 8000) {
                prunedBuffer.push(buffer[i]);
            } else {
                otherAliasBufferLength -= otherAliasBuffer.some(otherItem => otherItem.data === buffer[i].data) ? 0 : 1;
            }
        }

        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
        if (otherAliasBufferLength !== otherAliasBuffer.length) {
            setAliasData(prevState => ({ ...prevState, [otherAliasBuffer[0]]: otherAliasBufferLength }));
        }
    }, []);

    const handleNewData = useCallback((newData) => {
        const { alias, data } = newData;
        const buffer = aliasBufferRef.current[alias];
        const otherAliasBuffer = aliasBufferRef.current[alias === 'Zone1' ? 'Zone2' : 'Zone1'];

        const existingIndex = buffer?.findIndex(item => item.data === data);
        if (existingIndex === -1) {
            buffer.push({ data, timestamp: Date.now() });
            setTempAliasData(prevState => ({ ...prevState, [alias]: prevState[alias] + 1 }));
        } else {
            buffer[existingIndex].timestamp = Date.now();
        }

        const bufferToRemoveFrom = otherAliasBuffer;
        const bufferToRemoveFromDataIndex = bufferToRemoveFrom?.findIndex(item => item.data === data);
        if (bufferToRemoveFromDataIndex !== -1) {
            bufferToRemoveFrom.splice(bufferToRemoveFromDataIndex, 1);
        }

        updateAliasData(alias, aliasBufferRef.current[alias]);
        pruneBuffer(aliasBufferRef.current[alias], alias, otherAliasBuffer);
        pruneBuffer(otherAliasBuffer, alias === 'Zone1' ? 'Zone2' : 'Zone1', aliasBufferRef.current[alias]);

        setTempAliasData(prevState => ({ ...prevState, [alias]: 1 }));
    }, [updateAliasData, pruneBuffer]);

    useEffect(() => {
        const client = mqtt.connect('ws://127.0.0.1:9001');
        client.on('connect', () => {
            console.log('Connected to MQTT server');
            client.subscribe('test', (err) => {
                if (!err) {
                    console.log('Subscribed to test');
                }
            });
        });

        client.on('error', (error) => {
            console.error('Failed to connect to MQTT server:', error);
        });

        client.on('message', (topic, message) => {
            if (topic === 'test') {
                const jsonData = JSON.parse(message);
                handleNewData({
                    alias: jsonData?.alias,
                    data: jsonData?.data
                });
                setLastDataReceivedTime(prevState => ({ ...prevState, [jsonData?.alias]: Date.now() }));
            }
        });

        const updateTotalReceivedData = setInterval(() => {
            setTotalReceivedData(tempAliasData);
            setTempAliasData({ "Zone1": 0, "Zone2": 0 });
        }, 1000);

        const checkDataTimeout = setInterval(() => {
            const currentTime = Date.now();
            for (const alias in lastDataReceivedTime) {
                if (currentTime - lastDataReceivedTime[alias] > 1000) {
                    pruneBuffer(aliasBufferRef.current[alias], alias, aliasBufferRef.current[alias === 'Zone1' ? 'Zone2' : 'Zone1']);
                }
            }
        }, 1000);

        return () => {
            clearInterval(updateTotalReceivedData);
            clearInterval(checkDataTimeout);
        };
    }, [handleNewData, lastDataReceivedTime, pruneBuffer, tempAliasData]);

    return (
        <>
            <h1 style={{ color: "black", textAlign: "center", fontSize: "50px" }}>Mojix demo</h1>
            <div className='demo'>
                <div className="main-demo" style={{ textAlign: "center", fontSize: "20px" }}>
                    <div >
                        Zone 1
                        <div className="circle1">
                            {aliasData["Zone1"]}
                        </div>
                    </div>
                    <div >
                        Zone 2
                        <div className="circle2">
                            {aliasData["Zone2"]}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default App;