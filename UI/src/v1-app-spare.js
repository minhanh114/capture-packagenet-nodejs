/* eslint-disable no-unused-vars */
import React, { useEffect, useState, useCallback, useRef } from 'react';
import mqtt from 'mqtt';
import "./scss/demo.scss";

function App() {
    const [aliasData, setAliasData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0, "Zone4": 0 });
    const aliasBufferRef = useRef({ "Zone1": [], "Zone2": [], "Zone3": [], "Zone4": [] });
    const [lastDataReceivedTime, setLastDataReceivedTime] = useState({ "Zone1": Date.now(), "Zone2": Date.now(), "Zone3": Date.now(), "Zone4": Date.now() });
    const [tempAliasData, setTempAliasData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0, "Zone4": 0 });
    const [totalReceivedData, setTotalReceivedData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0, "Zone4": 0 });

    const updateAliasData = useCallback((alias, buffer) => {
        const prunedBuffer = buffer.filter(item => Date.now() - item.timestamp <= 4000);
        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
    }, []);

    const pruneBuffer = useCallback((buffer, alias, otherAliasBuffer) => {
        const currentTime = Date.now();
        const prunedBuffer = [];
        let otherAliasBufferLength = otherAliasBuffer.length;

        for (let i = 0; i < buffer.length; i++) {
            if (currentTime - buffer[i].timestamp <= 10000) {
                prunedBuffer.push(buffer[i]);
            } else {
                otherAliasBufferLength -= otherAliasBuffer.some(otherItem => otherItem.data === buffer[i].data) ? 0 : 1;
            }
        }

        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
        if (otherAliasBufferLength !== otherAliasBuffer.length) {
            setAliasData(prevState => ({ ...prevState, [otherAliasBuffer[0]]: otherAliasBufferLength }));
        }
    }, []);

    const handleNewData = useCallback((newData) => {
        const { alias, data } = newData;
        const buffer = aliasBufferRef.current[alias];
        const otherAliasBuffer1 = aliasBufferRef.current[alias === 'Zone1' ? 'Zone2' : 'Zone1'];
        const otherAliasBuffer2 = aliasBufferRef.current[alias === 'Zone3' ? 'Zone2' : 'Zone3'];
        const otherAliasBuffer3 = aliasBufferRef.current[alias === 'Zone4' ? 'Zone3' : 'Zone4'];

        const existingIndex = buffer?.findIndex(item => item.data === data);
        if (existingIndex === -1) {
            buffer.push({ data, timestamp: Date.now() });
            setTempAliasData(prevState => ({ ...prevState, [alias]: (prevState[alias] || 0) + 1 }));
        } else {
            buffer[existingIndex].timestamp = Date.now();
        }

        const bufferToRemoveFrom1 = otherAliasBuffer1;
        const bufferToRemoveFrom2 = otherAliasBuffer2;
        const bufferToRemoveFrom3 = otherAliasBuffer3;

        const bufferToRemoveFrom1DataIndex = bufferToRemoveFrom1?.findIndex(item => item.data === data);
        if (bufferToRemoveFrom1DataIndex !== -1) {
            bufferToRemoveFrom1.splice(bufferToRemoveFrom1DataIndex, 1);
        }

        const bufferToRemoveFrom2DataIndex = bufferToRemoveFrom2?.findIndex(item => item.data === data);
        if (bufferToRemoveFrom2DataIndex !== -1) {
            bufferToRemoveFrom2.splice(bufferToRemoveFrom2DataIndex, 1);
        }

        const bufferToRemoveFrom3DataIndex = bufferToRemoveFrom3?.findIndex(item => item.data === data);
        if (bufferToRemoveFrom3DataIndex !== -1) {
            bufferToRemoveFrom3.splice(bufferToRemoveFrom3DataIndex, 1);
        }

        updateAliasData(alias, aliasBufferRef.current[alias]);
        pruneBuffer(aliasBufferRef.current[alias], alias, otherAliasBuffer1, otherAliasBuffer2, otherAliasBuffer3);
        pruneBuffer(otherAliasBuffer1, alias === 'Zone1' ? 'Zone2' : (alias === 'Zone2' ? 'Zone3' : (alias === 'Zone3' ? 'Zone4' : 'Zone1')), aliasBufferRef.current[alias], otherAliasBuffer2, otherAliasBuffer3);
        pruneBuffer(otherAliasBuffer2, alias === 'Zone2' ? 'Zone1' : (alias === 'Zone3' ? 'Zone2' : (alias === 'Zone4' ? 'Zone3' : 'Zone4')), aliasBufferRef.current[alias], otherAliasBuffer1, otherAliasBuffer3);
        pruneBuffer(otherAliasBuffer3, alias === 'Zone1' ? 'Zone4' : (alias === 'Zone2' ? 'Zone1' : (alias === 'Zone3' ? 'Zone2' : 'Zone3')), aliasBufferRef.current[alias], otherAliasBuffer1, otherAliasBuffer2);

        setTempAliasData(prevState => ({ ...prevState, [alias]: 1 }));
    }, [updateAliasData, pruneBuffer]);



    useEffect(() => {
        const client = mqtt.connect('ws://127.0.0.1:9001');
        client.on('connect', () => {
            console.log('Connected to MQTT server');
            client.subscribe('test', (err) => {
                if (!err) {
                    console.log('Subscribed to test/topic');
                }
            });
        });

        client.on('error', (error) => {
            console.error('Failed to connect to MQTT server:', error);
        });

        client.on('message', (topic, message) => {
            if (topic === 'test') {
                const jsonData = JSON.parse(message);
                handleNewData({
                    alias: jsonData?.alias,
                    data: jsonData?.data
                });
                setLastDataReceivedTime(prevState => ({ ...prevState, [jsonData?.alias]: Date.now() }));
            }
        });

        const updateTotalReceivedData = setInterval(() => {
            setTotalReceivedData(tempAliasData);
            setTempAliasData({ "Zone1": 0, "Zone2": 0, "Zone3": 0, "Zone4": 0 });
        }, 1000);

        const checkDataTimeout = setInterval(() => {
            const currentTime = Date.now();
            for (const alias in lastDataReceivedTime) {
                if (currentTime - lastDataReceivedTime[alias] > 1000) {
                    let otherAlias;
                    switch (alias) {
                        case 'Zone1':
                            otherAlias = 'Zone2';
                            break;
                        case 'Zone2':
                            otherAlias = 'Zone3';
                            break;
                        case 'Zone3':
                            otherAlias = 'Zone4';
                            break;
                        case 'Zone4':
                            otherAlias = 'Zone1';
                            break;
                        default:
                            break;
                    }
                    pruneBuffer(aliasBufferRef.current[alias], alias, aliasBufferRef.current[otherAlias]);
                }
            }
        }, 1000);

        return () => {
            clearInterval(updateTotalReceivedData);
            clearInterval(checkDataTimeout);
        };
    }, [handleNewData, lastDataReceivedTime, pruneBuffer, tempAliasData]);

    return (
        <>
            <h1 style={{ color: "black", textAlign: "center", fontSize: "50px" }}>Mojix demo</h1>
            <div className='demo'>
                <div className="main-demo" style={{ textAlign: "center", fontSize: "20px" }}>
                    <div >
                        Zone 1
                        <div className="circle1">
                            {aliasData["Zone1"]}
                        </div>
                    </div>
                    <div >
                        Zone 2
                        <div className="circle2">
                            {aliasData["Zone2"]}
                        </div>
                    </div>
                    <div >
                        Zone 3
                        <div className="circle3">
                            {aliasData["Zone3"]}
                        </div>
                    </div>
                    <div >
                        Zone 4
                        <div className="circle4">
                            {aliasData["Zone4"]}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default App;