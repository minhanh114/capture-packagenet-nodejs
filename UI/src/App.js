/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useCallback, useRef } from 'react';
import { fetchEventSource } from '@microsoft/fetch-event-source';
//import mqtt from 'mqtt';
import "./scss/demo.scss";

function App() {
    const [aliasData, setAliasData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0 });
    const aliasBufferRef = useRef({ "Zone1": [], "Zone2": [], "Zone3": [] });
    const [lastDataReceivedTime, setLastDataReceivedTime] = useState({ "Zone1": Date.now(), "Zone2": Date.now(), "Zone3": Date.now() });
    const [tempAliasData, setTempAliasData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0 });
    const [totalReceivedData, setTotalReceivedData] = useState({ "Zone1": 0, "Zone2": 0, "Zone3": 0 });
    const [searchId, setSearchId] = useState('');
    const [searchResult, setSearchResult] = useState('');
    const [dataArray, setDataArray] = useState([]);
    const serverBaseURL = 'http://172.20.10.12'

    const updateAliasData = useCallback((alias, buffer) => {
        const prunedBuffer = buffer.filter(item => Date.now() - item.timestamp <= 5000);
        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
    }, []);

    const pruneBuffer = useCallback((buffer, alias, otherAliasBuffer) => {
        const currentTime = Date.now();
        const prunedBuffer = [];
        let otherAliasBufferLength = otherAliasBuffer.length;

        for (let i = 0; i < buffer.length; i++) {
            if (currentTime - buffer[i].timestamp <= 3000) { //Set time at increase tags or change distance read
                prunedBuffer.push(buffer[i]);
            } else {
                otherAliasBufferLength -= otherAliasBuffer.some(otherItem => otherItem.data === buffer[i].data) ? 0 : 1;
                setDataArray(prevDataArray => prevDataArray.filter(item => item?.data !== buffer[i]?.data));
            }
        }

        setAliasData(prevState => ({ ...prevState, [alias]: prunedBuffer.length }));
        if (otherAliasBufferLength !== otherAliasBuffer.length) {
            setAliasData(prevState => ({ ...prevState, [otherAliasBuffer[0]]: otherAliasBufferLength }));
        }
    }, []);

    const handleNewData = useCallback((newData) => {
        const { alias, data, timestamp } = newData;
        const buffer = aliasBufferRef.current[alias];
        const otherAlias1 = alias === 'Zone1' ? 'Zone2' : alias === 'Zone2' ? 'Zone3' : 'Zone1';
        const otherAlias2 = alias === 'Zone1' ? 'Zone3' : alias === 'Zone2' ? 'Zone1' : 'Zone2';
        const otherAliasBuffer1 = aliasBufferRef.current[otherAlias1];
        const otherAliasBuffer2 = aliasBufferRef.current[otherAlias2];

        // Clear data for previous zone
        setTempAliasData(prevState => ({ ...prevState, [alias]: 0 }));

        const existingIndex = buffer.findIndex(item => item.data === data);
        if (existingIndex === -1) {
            buffer.push({ data, timestamp: Date.now() });
            setTempAliasData(prevState => ({ ...prevState, [alias]: 1 }));
        } else {
            buffer[existingIndex].timestamp = Date.now();
        }

        // Update other zone's buffer
        const otherAliasBufferIndex1 = otherAliasBuffer1.findIndex(item => item.data === data);
        if (otherAliasBufferIndex1 !== -1) {
            otherAliasBuffer1.splice(otherAliasBufferIndex1, 1);
        }

        const otherAliasBufferIndex2 = otherAliasBuffer2.findIndex(item => item.data === data);
        if (otherAliasBufferIndex2 !== -1) {
            otherAliasBuffer2.splice(otherAliasBufferIndex2, 1);
        }

        updateAliasData(alias, buffer);
        updateAliasData(otherAlias1, otherAliasBuffer1);
        updateAliasData(otherAlias2, otherAliasBuffer2);
        pruneBuffer(buffer, alias, otherAliasBuffer1, otherAliasBuffer2);
        // Update data array state
        setDataArray(prevDataArray => {
            const existingDataIndex = prevDataArray.findIndex(item => item?.data === data);

            if (existingDataIndex !== -1) {
                // If data already exists, update its alias and timestamp
                prevDataArray[existingDataIndex].alias = alias;
                prevDataArray[existingDataIndex].timestamp = timestamp;
                return [...prevDataArray];
            } else {
                // If data does not exist, add it to the array
                return [...prevDataArray, { data, alias, timestamp, id: data.slice(4, -4) }];
            }
        });

    }, [updateAliasData, pruneBuffer]);

    useEffect(() => {
        const fetchData = async () => {
            await fetchEventSource(`${serverBaseURL}/rfid/events`, {
                method: "GET",
                /*  headers: {
                     "Content-Type": "text/event-stream",
                     "Cache-Control": "no-cache",
                     "Connection": "keep-alivesss"
                 }, */
                onopen(res) {
                    if (res.ok && res.status === 200) {
                        console.log("Connection made ", res);
                    } else if (
                        res.status >= 400 &&
                        res.status < 500 &&
                        res.status !== 429
                    ) {
                        console.log("Client side error ", res);
                    }
                },
                onmessage(event) {
                    const data = event.data;
                    if (data.trim() === '') {
                        console.error('Received empty JSON data');
                        return;
                    }
                    try {
                        const parsedData = JSON.parse(data);
                        if (parsedData.type === 'TagReadData') {
                            handleNewData(parsedData);
                            setLastDataReceivedTime(prevState => ({ ...prevState, [parsedData?.alias]: Date.now() }));
                        }
                    } catch (error) {
                        console.error('Error parsing JSON data:', error);
                    }
                },

                onclose() {
                    console.log("Connection closed by the server");
                },
                onerror(err) {
                    /* TODO: Temporary demo method, because when moving over a tab, the other application then automatically reconnects but a CORS error occurs.
                    To cover the problem of saving results when moving through a tab, application and then back the other way, study the API documentation */
                    window.location.reload()
                    console.log("There was an error from server", err);
                },
            });
        }
        fetchData();
    }, []);

    useEffect(() => {
        const updateTotalReceivedData = setInterval(() => {
            setTotalReceivedData(prevState => ({ ...prevState, ...tempAliasData }));
            setTempAliasData({ "Zone1": 0, "Zone2": 0, "Zone3": 0 });
        }, 1000);

        const checkDataTimeout = setTimeout(() => {
            const currentTime = Date.now();
            for (const alias in lastDataReceivedTime) {
                if (currentTime - lastDataReceivedTime[alias] > 2000) {
                    const otherAlias1 = alias === 'Zone1' ? 'Zone2' : alias === 'Zone2' ? 'Zone3' : 'Zone1';
                    const otherAlias2 = alias === 'Zone1' ? 'Zone3' : alias === 'Zone2' ? 'Zone1' : 'Zone2';
                    pruneBuffer(aliasBufferRef.current[alias], alias, aliasBufferRef.current[otherAlias1], aliasBufferRef.current[otherAlias2]);
                }
            }
        }, 1000);

        return () => {
            clearInterval(updateTotalReceivedData);
            clearTimeout(checkDataTimeout);
        };
    }, [aliasData, lastDataReceivedTime, pruneBuffer]);

    const handleSearchIdChange = (event) => {
        setSearchId(event.target.value);
    };

    const handleFindButtonClick = () => {
        const cleanedSearchId = searchId.trim();

        if (cleanedSearchId === '') {
            setSearchResult('Please enter a valid EANCode');
            return;
        }

        const searchData = dataArray.find(item => item?.id === cleanedSearchId);

        if (searchData) {
            setSearchResult(
                <span>
                    {' '}
                    Result for EANCode{' '}
                    {cleanedSearchId} {' '}
                    is in {' '}
                    <span style={{ color: 'red', fontWeight: 'bold', fontSize: '20px' }}>
                        {searchData.alias}
                    </span>
                </span>
            );
        } else {
            setSearchResult(` Result for EANCode ${cleanedSearchId} not found`);
        }
    };

    return (
        <>
            <h1 style={{ color: "black", textAlign: "center", fontSize: "35px" }}>Mojix demo</h1>
            <div style={{ marginTop: '20px', marginLeft: '100px', width: '88%' }}>
                <div className='demo'>
                    <div className="main-demo" style={{ textAlign: "center", fontSize: "20px" }}>
                        <div >
                            Zone 1
                            <div className="circle1">
                                {aliasData["Zone1"]}
                            </div>
                        </div>
                        <div >
                            Zone 2
                            <div className="circle2">
                                {aliasData["Zone2"]}
                            </div>
                        </div>
                        <div >
                            Zone 3
                            <div className="circle3">
                                {aliasData["Zone3"]}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ marginTop: '20px', marginLeft: '100px' }}>
                <div className="search-container">
                    <input className="search-input" type="text" value={searchId} onChange={handleSearchIdChange} />
                    <button className="find-button" onClick={handleFindButtonClick}>Find asset</button>
                </div>
                <label className="result-label">{searchResult}</label>

            </div>
            <div style={{ marginTop: '20px', marginLeft: '100px', width: '88%' }}>
                <div className="table-container">
                    <div className="header-container">
                        <table style={{ borderCollapse: 'collapse', width: '100%', backgroundColor: '#BDBDBD' }}>
                            <thead>
                                <tr>
                                    <th style={{ padding: '5px', width: '3%' }}>No</th>
                                    <th style={{ padding: '5px', width: '45%' }}>EANCode</th>
                                    <th style={{ padding: '5px', width: '45%' }}>Time</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div className="body-container">
                        <table style={{ borderCollapse: 'collapse', width: '100%', textAlign: 'center' }}>
                            <tbody>
                                {dataArray.map((item, index) => (
                                    <tr key={index}>
                                        <td style={{ padding: '7px', width: '3%' }}>{index + 1}</td>
                                        <td style={{ padding: '7px', width: '45%' }}>{item?.id}</td>
                                        <td style={{ padding: '7px', width: '45%' }}>{new Date(item?.timestamp).toLocaleString()}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>
    );
}

export default App;
