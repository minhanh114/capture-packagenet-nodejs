const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
const mqtt = require('mqtt');

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

// Kết nối tới máy chủ MQTT
const mqttClient = mqtt.connect('mqtt://127.0.0.1:1883');

mqttClient.on('connect', function () {
    console.log('Connected to MQTT broker');
});
// Thêm sự kiện 'test'
io.on('connection', function (socket) {
    console.log('A user connected with ID:', socket.id);
    // Lắng nghe sự kiện 'test' từ client
    socket.on('test', function (data) {
        const jsonData = JSON.parse(data);
        if (jsonData.alias && jsonData.data) {
            const newData = {
                alias: jsonData.alias,
                data: jsonData.data,
                timestamp: jsonData.timestamp
            }
            const result = JSON.stringify(newData)
            mqttClient.publish('test', result);
        }
    });
});

io.on('disconnect', function (socket) {
    console.log('A user disconnected with ID:', socket.id);
});

server.listen(3000, function () {
    console.log('server is up!');
});
