from scapy.all import sniff, Raw
import socketio
import json
import re

sio = socketio.Client()

def packet_callback(packet):
    try:
        # Check if the packet has a payload
        if packet.haslayer(Raw):
            # Extract the payload
            payload = packet[Raw].load

            # Convert payload to string
            payload_str = payload.decode('utf-8')

            match = re.search(r'\{.*"type":"TagReadData".*[^RoundStart]*\}', payload_str)
            if match:
                json_str = match.group()
                data_json = json.loads(json_str)
                sio.emit('test', json_str)  # Send JSON string directly
    except Exception as e:
        print("Error processing packet:", e)

@sio.event
def connect():
    print("Connected to the server.")

@sio.event
def connect_error(error):
    print("Connection to the server failed:", error)

if __name__ == "__main__":
    sio.connect('http://localhost:3000')

    # Bắt đầu lắng nghe các gói tin TCP trên giao diện được chỉ định
    sniff(iface="Ethernet 2", prn=packet_callback, filter="tcp")

    # Giữ client chạy
    sio.wait()
